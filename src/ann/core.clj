(ns ann.core
  (:gen-class))

(defn calculate-activation [net]
  (/ 1 (+ 1 (Math/exp (- net))))
  )

(defn calculate-net
  ([weights bias inputs]
    (+ (reduce + (map * weights inputs)) bias)
    )
  )

(defn generate-random-wages
  "Generates random wages for neuron"
  ([size] (generate-random-wages '() size))
  ([wages size]
    (if (== size 0) wages
      (generate-random-wages (conj wages (rand 1)) (dec size))
      )
    )
  )

(defn calculate-activation-for-neuron
  [input neuron]
  (calculate-activation (calculate-net (neuron :weights) (neuron :bias) input))
  )

(defn create-neuron [noOfInputs name]
  {:name name :weights (generate-random-wages noOfInputs) :bias (rand 1)}
  )

(defn create-layer [noOfNeurons noOfInputs layerName]
  (for  [x (range 0 noOfNeurons)]
    (create-neuron noOfInputs (str layerName " - " x))
    )
  )

(defn create-network [noOfInputs outputNumber]
  (def neuronsInOutputLayer (Math/ceil (Math/sqrt outputNumber)))
  (conj '() (create-layer neuronsInOutputLayer noOfInputs "output") (create-layer noOfInputs noOfInputs "hidden"))
  )

(defn get-activation-for-all-neurons
  [neurons input]
  (let [calculate-activation-for-neuron-partial (partial calculate-activation-for-neuron input)]
    (map calculate-activation-for-neuron-partial neurons)
    )
  )

(defn get-output-layer-deltas [expectedResult outputLayerResult]
  (let [errorFactors (map - expectedResult outputLayerResult)]
    (map #(* %1 (- 1 %1) %2) outputLayerResult errorFactors)
    )
  )

(defn get-hidden-layer-error-factors [hiddenLayer outputLayerDeltas]
  (map #(reduce + (map * (% :weights) outputLayerDeltas)) hiddenLayer)
  )

(defn get-hidden-layer-deltas [hiddenLayerResult errorFactors]
  (map #(* %1 (- 1 %1) %2) hiddenLayerResult errorFactors)
  )

(defn calculate-new-bias [oldValue learningRate neuronDelta]
  (+ oldValue (* learningRate 1 neuronDelta))
  )

(defn update-sigle-neuron-weights [weights neuronDelta previousOutputs learningRate]
  ; New Weight  = Old Weight +  LEARNING_RATE * 1 * Output Of InputNeuron * Delta
  (map #(+ %1 (* learningRate 1 %2 neuronDelta)) weights previousOutputs)
  )

(defn flattenWeights [layer]
  (reduce #(conj %1 (%2 :weights)) [] layer)
  )

(defn rebuild-neuron [inputs learningRate neuron delta]
  (let [newWeights (update-sigle-neuron-weights (neuron :weights) delta inputs learningRate)
        newBias (calculate-new-bias (neuron :bias) learningRate delta)]
    (assoc neuron :weights (seq newWeights) :bias newBias)
    )
  )

(defn rebuild-layer [neurons neuronDeltas inputs learningRate]
  (let [rebuild-neuron-map (partial rebuild-neuron inputs learningRate)]
    (map rebuild-neuron-map  neurons neuronDeltas)
    )
  )

(defn train-network-using-single-sample [network trainingSample learningRate]
  (let [hiddenLayer (first network)
        outputLayer (last network)
        hiddenLayerResult (get-activation-for-all-neurons hiddenLayer (trainingSample :input))
        outputLayerResult (get-activation-for-all-neurons outputLayer hiddenLayerResult)
        outputLayerDeltas (get-output-layer-deltas (trainingSample :expectedResult) outputLayerResult)
        hiddenLayerErrorFactors (get-hidden-layer-error-factors hiddenLayer outputLayerDeltas)
        hiddenLayerDeltas (get-hidden-layer-deltas hiddenLayerResult hiddenLayerErrorFactors)
        updatedOutputLayer (rebuild-layer outputLayer outputLayerDeltas hiddenLayerResult learningRate)
        updatedHiddenLayer (rebuild-layer hiddenLayer hiddenLayerDeltas (trainingSample :input) learningRate)]
    (conj '() (seq updatedOutputLayer) (seq updatedHiddenLayer))
    )
  )

(defn decimal-to-binary [number size]
  ((fn build[binaryList num factor]
     (if (< factor 0)
       binaryList
       (let [powedNumber (Math/pow 2 factor)]
         (if (>= num powedNumber)
           (build (conj binaryList 1) (- num powedNumber) (dec factor))
           (build (conj binaryList 0) num (dec factor))
           )
         )
       )
     ) [] number (dec size))
  )

(defn enrich-training-set-with-expected-result [trainingSet]
  (let [expectedOutputArraySize (Math/round (Math/sqrt (count trainingSet)))]

    ((fn updateTrainingSetRecur [trainingSet updatedTrainingSet outputExpectedValue]

       (if (empty? trainingSet)
        updatedTrainingSet

        (updateTrainingSetRecur
          (rest trainingSet)
          (conj updatedTrainingSet (assoc (first trainingSet) :expectedResult (decimal-to-binary outputExpectedValue expectedOutputArraySize)))
          (inc outputExpectedValue)
          )
        )

      ) trainingSet [] 0)
    )
  )

(defn train-network [network trainingSet learningRate counter]
  (loop [countDown counter
         trainedNetwork network]
    (if (= countDown 0)
      trainedNetwork
      (recur
        (dec countDown)
        (reduce #(train-network-using-single-sample %1 %2 learningRate) trainedNetwork trainingSet)
        )
      )
    )
  )

; OUTPUT functions

(defn get-output [network input]
  (let [hiddenLayer (first network)
        outputLayer (last network)
        hiddenLayerResult (get-activation-for-all-neurons hiddenLayer input)
        outputLayerResult (get-activation-for-all-neurons outputLayer hiddenLayerResult)]
    outputLayerResult
    )
  )

(defn get-distance [output patternOutput]
  (reduce + (map #(Math/abs (- %1 %2)) output patternOutput))
  )

(defn match-output-to-patterns [output samples]
  (let [distances (map #(get-distance output (%1 :expectedResult))  samples)
        pairs (map #(hash-map :distance %1 :symbol (%2 :symbol)) distances samples)
        pairsSorted (sort (fn[x y]
                            (< (x :distance)  (y :distance)))
                      pairs)
        ]
    pairsSorted
    )
  )