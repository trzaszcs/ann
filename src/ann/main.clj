(ns main
  (:require [ann.core :refer :all])
  (:require [ann.utils :as utils]))

(def network (create-network 9 1))
(def trainingSetWithExpectedResults (enrich-training-set-with-expected-result (utils/getTrainingSetFromFile "../../resources/trainingSet.json")))
(def trainedNetwork (train-network network trainingSetWithExpectedResults 0.7 12000))

;(println "=>" trainingSetWithExpectedResults)
;(println "=>" trainedNetwork)
(def output (get-output trainedNetwork [0 1 0 0 0 0 0 1 0]))
(println "-->" (match-output-to-patterns output trainingSetWithExpectedResults))