(ns ann.utils
  (:require [clojure.data.json :as json]))

(defn getTrainingSetFromFile [fileName]
  ((json/read-str (slurp fileName) :key-fn keyword) :trainingSet)
  )
