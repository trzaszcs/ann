(ns ann.core-test
  (:require [clojure.test :refer :all]
            [ann.core :refer :all]))


(deftest generating-random-wages
  (testing "generate-random-wages"
    (is (= 3 (count (generate-random-wages 3))))))


(deftest test-getting-activations-for-all-neurons
  (testing "get-activation-for-all-neurons"
    (let [activations (get-activation-for-all-neurons '({:weights (0.2 1.0) :bias 0.2} {:weights (0.1 0.2) :bias 0.1}) '(1 1))
          firstNeuronActivation (calculate-activation 1.4)
          secondNeuronActivation (calculate-activation 0.4)
          ]
      (is (seq? activations))
      (is (= 2 (count activations)))
      (is (= (conj '() secondNeuronActivation firstNeuronActivation) activations))
      )
    )
  )


(deftest ann

  (testing "create-neuron"
    (let [neuron (create-neuron 2 "name")]
      (is (= "name" (neuron :name)))
      (is (= 2 (count (neuron :weights))))
      (is (number? (neuron :bias)))
      )
    )

  (testing "calculate-net"
    (let [net (calculate-net [0.2 0.2] 3 [1 1])]
      (is (= 3.4 net))
      )
    )

  (testing "create-network"
    (let [network (create-network 3 1)
          hiddenLayer (first network)
          outputLayer (last network)

          ]
      (is (= 3 (count hiddenLayer)))
      (is (= 3 (count ((first outputLayer) :weights))))
      (is (= 1 (count outputLayer)))
      )
    )

  (testing "get-output-layer-deltas"
    (let [deltas (get-output-layer-deltas '(1 1) '(2 2))]
      (is (seq? deltas))
      (is (= '(2 2) deltas))
      )
    )

  (testing "get-hidden-layer-error-factors"
    (let [deltas (get-hidden-layer-error-factors '({:weights [1]} {:weights [1]}) [0.5])]
      (is (seq? deltas))
      (is (= '(0.5 0.5) deltas))
      )
    )

  (testing "get-hidden-layer-deltas"
    (let [deltas (get-hidden-layer-deltas [2 2] [1 1])]
      (is (seq? deltas))
      (is (= '(-2 -2) deltas))
      )
    )

  (testing "flattenWeights"
    (let [weights (flattenWeights [{:weights [1 2]} {:weights [3 4]} {:weights [5 6]}])]
      (is (= [[1 2] [3 4] [5 6]] weights))
      )
    )


  (testing "calculate-new-bias"
    (let [newBias (calculate-new-bias 1 1 0.5)]
      (is (= newBias 1.5))
      )
    )


  (testing "rebuild-neuron"
    (let [newNeuron (rebuild-neuron [1 1]  1 {:name "0" :weights [1 2] :bias 1} 0.5)]
      (is (= 1.5 (newNeuron :bias)))
      (is (= "0" (newNeuron :name)))
      (is (= '(1.5 2.5) (newNeuron :weights)))
      )
    )

  (testing "getDistance"
    (let [distance (getDistance [0 0] [1 1])]
      (is (= 2 distance))
      )
    )

  (testing "matchOutputToPatterns"
    (let [match (matchOutputToPatterns [0 0] [{:symbol "I" :expectedResult [0 1]} {:symbol "U" :expectedResult [0 0]}])]
      (is (= '({:distance 0 :symbol "U"} {:distance 1 :symbol "I"}) match))
      )
    )

  (testing "decimal-to-binary"
    (let [binary (decimal-to-binary 3 3)]
      (is (= [0 1 1] binary))
      )
    )

  (testing "enrich-training-set-with-expected-result"
    (let [updatedTrainingSet (enrich-training-set-with-expected-result [{} {}])]
      (is (= 2 (count updatedTrainingSet)))
      (is (= [0] ((nth updatedTrainingSet 0) :expectedResult)))
      (is (= [1] ((nth updatedTrainingSet 1) :expectedResult)))
      )
    )

  )

(run-tests)


