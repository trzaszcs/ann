(ns ann.core-test
  (:require [clojure.test :refer :all]
            [ann.utils :refer :all]))

(def trainingSetFilePath "../../resources/trainingSet.json")

(deftest getsTrainingSetFromFile
  (testing "getTrainingSetFromFile"
    (let [trainingSet (getTrainingSetFromFile trainingSetFilePath)]
      (is (= 2 (count trainingSet)))
      )
  ))


(run-tests)


